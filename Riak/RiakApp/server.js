const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const app = express();

// Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Test
app.get('/', (req, res) => {
    return res.send('Hello, this is a Riak TS backend on Node.js');
});

const PORT = process.env.PORT || 3000;
const MQ_URL = process.env.MQ_URL || "http://localhost:8080/message/consume";
const RIAK_URL = process.env.RIAK_URL || "127.0.0.1:8087";
const TRANSFORMER_URL = process.env.TRANSFORMER_URL || "http://localhost:8081/transformer";
const Riak = require('basho-riak-client');

const table = 'device_data';
const columns = [
    {name: 'device_id', type: Riak.Commands.TS.ColumnType.Varchar},
    {name: 'payload', type: Riak.Commands.TS.ColumnType.Varchar},
    {name: 'payload_raw', type: Riak.Commands.TS.ColumnType.Varchar},
    {name: 'time', type: Riak.Commands.TS.ColumnType.Timestamp},
];

// recursively try to connect to Riak
var client;
var connected = false;
connect();

function connect() {
    console.log("Connecting to Riak...");

    client = new Riak.Client([RIAK_URL], (err, client) => {
        if (err != null) {
            console.log("Failed to connect to Riak node");
            setTimeout(() => connect(), 10000);
            return;
        }

        connected = true;
        console.log("Successfully connected to Riak node");

        setTimeout(() => {
            // Create device_data table if not created yet
            let cb = (err, result) => {
                if (err != null) {
                    console.log("FAILED TO CREATE RIAK TABLE");
                    console.log(err);
                    return;
                }

                console.log("CREATE TABLE callback");
                console.log(result);
            };

            let cmd = new Riak.Commands.TS.Query.Builder()
                .withQuery(`
                CREATE TABLE ${table} (
                    device_id    varchar    not null,
                    payload      varchar    not null,
                    payload_raw  varchar    not null,
                    time         timestamp  not null,
                    PRIMARY KEY((device_id, quantum(time, 15, 'm')), device_id, time)
                );
            `)
                .withCallback(cb)
                .build();

            client.execute(cmd);
        }, 5000);
    });
}

// Test Riak connection
app.get('/ping', (req, res) => {
    client.ping((err, result) => {
        if (err) {
            throw new Error(err);
        } else {
            res.send(JSON.stringify({connected: result}));
        }
    });
});

// start the server
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});

// poll message queue for messages
setInterval(() => {
    if (!connected) {
        console.log("Riak not connected yet...");
        return;
    }

    axios.get(MQ_URL)
        .then(response => {
            let messages = response.data;
            if (messages.length === 0) {
                return;
            }

            messages.forEach((message) => {
                let payload = message.message;
                consumeMessage(payload);
            });
        })
        .catch(error => {
            console.log("Redis MQ connection error");
            console.log(error);
        });
}, 500);

// consume message from the message queue
function consumeMessage(payload) {
    axios.get(TRANSFORMER_URL)
        .then(response => {
            // TODO HACK
            let transformer = response.data[0].body.replace(/(\r\n|\n|\r)/gm, "");
            let result = transformMessage(payload, transformer);
            storeMessage(result.deviceId, result.payload, payload, result.time);
        })
        .catch(error => {
            console.log(error);
        });
}

// transform received message using the supplied transformer
function transformMessage(message, transformer) {
    console.log("Transforming...");
    console.log("Message:  " + message);
    console.log("Function: " + transformer);

    let result = eval(`(function() { 
        ${transformer}
        return { deviceId: deviceId, payload: payload, time: time };
    }())`);

    let deviceId = result.deviceId;
    let payload = result.payload;
    let time = result.time;

    return {deviceId, payload, time};
}

// stores message in the Riak TS database
function storeMessage(deviceId, payload, payloadRaw, time) {
    let rows = [[deviceId, payload, payloadRaw, time]];

    console.log(rows);

    let cb = function (err, response) {
        if (err != null) {
            console.log(err);
            return;
        }

        console.log("STORE ROW callback");
        console.log(response);
    };

    let store = new Riak.Commands.TS.Store.Builder()
        .withTable(table)
        .withColumns(columns)
        .withRows(rows)
        .withCallback(cb)
        .build();

    client.execute(store);
}
