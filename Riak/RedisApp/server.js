const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Test
app.get('/', (req, res) => {
    return res.send('Hello, this is a simple Redis message broker (:');
});

const PORT = process.env.PORT || 3000;
const QUEUE_NAME = process.env.QUEUE || "device_data_queue";

const RedisSMQ = require("rsmq");
const rsmq = new RedisSMQ({host: "redis", port: 6379, ns: "rsmq"});

// create
rsmq.createQueue({qname: QUEUE_NAME}, (err, resp) => {
    if (err) {
        console.log(JSON.stringify({error: err}));
        return;
    }

    if (resp === 1) {
        console.log("Queue successfully created");
    } else {
        console.log("Failed to create queue");
    }
});

// get a list of queues
app.get('/queues', (req, res) => {
    rsmq.listQueues((err, queues) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        res.send(JSON.stringify(queues));
    });
});

// get queue parameters
app.get('/queue_attrs/:name', (req, res) => {
    const {name} = req.params;
    rsmq.getQueueAttributes({qname: name}, (err, resp) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        res.send(JSON.stringify(resp));
    });
});

// publish a message to queue
app.post('/message/publish', (req, res) => {
    rsmq.sendMessage({qname: QUEUE_NAME, message: JSON.stringify(req.body)}, (err, resp) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return
        }

        let response = {messageId: resp};
        res.send(JSON.stringify(response));
    });
});

// pop a message from the queue
app.get('/message/consume', (req, res) => {
    rsmq.popMessage({qname: QUEUE_NAME}, (err, resp) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        if (resp.id) {
            res.send(JSON.stringify([resp]));
        } else {
            res.send(JSON.stringify([]));
        }
    });
});

// receive a message from the queue
app.get('/message/receive', (req, res) => {
    rsmq.receiveMessage({qname: QUEUE_NAME}, (err, resp) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        if (resp.id) {
            res.send(JSON.stringify([resp]));
        } else {
            res.send(JSON.stringify([]));
        }
    });
});

// delete a message from the queue
app.get('/message/delete/:messageId', (req, res) => {
    const {messageId} = req.params;
    rsmq.deleteMessage({qname: "myqueue", id: messageId}, (err, resp) => {
        if (err) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        res.send("OK");
    });
});

// start the server
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
