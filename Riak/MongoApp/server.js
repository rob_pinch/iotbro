const express = require('express');
const bodyParser = require('body-parser');
const app = express();

// Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Test
app.get('/', (req, res) => {
    return res.send('Hello, this is a MongoDB backend on Node.js');
});

const PORT = process.env.PORT || 3000;
const MONGO_URL = process.env.MONGO_URL || "mongodb://localhost:27017/";

const DATABASE = "mongo";
const COLLECTION = "transformers";
const TRANSFORMER_NAME = "mybro";

const Mongo = require('mongodb').MongoClient;
let client;

// Connect to Mongo DB
console.log("Connecting to mongo at " + MONGO_URL);

// recursively try to connect to MongoDB
connect();
function connect() {
    console.log("Connecting to Mongo...");

    Mongo.connect(MONGO_URL, (err, db) => {
        if (err != null) {
            console.log("Failed to connect to MongoDB");
            setTimeout(() => connect(), 5000);
            return;
        }

        console.log("Connected to MongoDB");
        client = db.db(DATABASE);

        client.createCollection(COLLECTION, (err, result) => {
            if (err != null) {
                console.log("Failed to create a MongoDB collection");
                return;
            }

            console.log("MongoDB Collection is created");
        });
    });
}

// Test Mongo connection
app.get('/ping', (req, res) => {
    res.send(JSON.stringify({connected: client != null}));
});

// get all transformation functions
app.get('/transformer', (req, res) => {
    client.collection(COLLECTION).find({}).toArray(function (err, result) {
        if (err != null) {
            res.status(500).send(JSON.stringify(err));
            return;
        }

        res.send(JSON.stringify(result));
    });
});

// update the transformation function
app.post('/transformer', (req, res) => {
    let _name = TRANSFORMER_NAME;
    let _body = req.body.functionBody;

    client.collection(COLLECTION).updateOne(
        {
            name: _name
        },
        {
            $set: {
                name: _name,
                body: _body
            }
        },
        {
            upsert: true
        },
        (err, obj) => {
            if (err) {
                console.log(err);
                res.status(500).send(JSON.stringify(err));
                return;
            }

            res.send(obj);
        }
    );
});

// start the server
app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`);
});
